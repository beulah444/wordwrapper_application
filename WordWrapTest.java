package source;

import org.junit.Test;
import source.WordWrapper;
import junit.framework.Assert;

public class WordWrapTest {

	//Test1,first test is if empty text then output should be "The given string is Empty"
	//it fails since no code written yet
	@Test
	public void test_whenIsEmpty_thenEmptyStringMsgIsReturned() {
		Assert.assertEquals("The given string is Empty",WordWrapper.wrap("", 54));
	}
	//Test2, when you give a null input the output a corresponding error message
	@Test
	public void test_whenIsNull_thenNullStringMsgIsReturned() {
		Assert.assertEquals("The given string is Null",WordWrapper.wrap(null, 54));
	}
	//Test3, when you give a string input the output a corresponding error message with max of 54 characters output with break test on word boundaries
	@Test
	public void test_whenIsGreaterthanMaxLength_thenGreaterMxLengthStringMsgIsReturned() {
		Assert.assertEquals("Women’s Leadership Network (WLN)is Henry Schein’s\r\n" + 
				"first Employee Resource Group (ERG), which is a\r\n" + 
				"voluntary, employee-led network that fosters a\r\n" + 
				"diverse, inclusive workplace aligned with the\r\n" + 
				"Company’s Corporate Charter, Team Schein Values, and\r\n" + 
				"business goals. The mission of WLN is to provide a\r\n" + 
				"forum that empowers, develops, and connects a network\r\n" + 
				"of women to drive business success. The group is open\r\n" + 
				"to all Team Schein Members, regardless of gender and\r\n" + 
				"level, and participation is encouraged from all",WordWrapper.wrap("Women’s Leadership Network (WLN)is Henry Schein’s first Employee Resource Group (ERG), which is a voluntary, employee-led network that fosters a diverse, inclusive workplace aligned with the Company’s Corporate Charter, Team Schein Values, and business goals. The mission of WLN is to provide a forum that empowers, develops, and connects a network of women to drive business success. The group is open to all Team Schein Members, regardless of gender and level, and participation is encouraged from all", 54));
	}
		@Test
	public void test_whenIsnotgreater_thenAssignTheOriginalTextAsItisAndReturned() {
		Assert.assertEquals("Women’s Leadership Network (WLN)is Henry Schein’s"
				,WordWrapper.wrap("Women’s Leadership Network (WLN)is Henry Schein’s", 100));
	}
	@Test
	public void test_whenMoreSpacesatEnd_thenAssignTheOriginalTextAsItisAndReturned() {
		Assert.assertEquals("Women’s Leadership Network (WLN)is Henry Schein’s"
				,WordWrapper.wrap("Women’s Leadership Network (WLN)is Henry Schein’s      ", 100));
	}
	
	@Test
	public void test_whenMoreSpacesatLineBegining_thenAssignTheOriginalTextAsItisAndReturned() {
		Assert.assertEquals("Women’s Leadership Network (WLN)is Henry Schein’s"
				,WordWrapper.wrap("    Women’s Leadership Network (WLN)is Henry Schein’s", 100));
	}
	@Test
	public void test_WhitespacesBeginAndEnd() {
		Assert.assertEquals("Hey  there    Henry Schein’s!!!"
				,WordWrapper.wrap("      Hey  there    Henry Schein’s!!!      ", 100));
	}
		@Test
	public void test_spacesInbeginandEnd_longstring() {
		Assert.assertEquals("Women’s Leadership Network (WLN)is Henry Schein’s\r\n" +
				"first Employee Resource Group (ERG), which is a\r\n" +
				"voluntary, employee-led network that fosters a\r\n" +
				"diverse, inclusive workplace aligned with the\r\n" +
				"Company’s Corporate Charter, Team Schein Values, and\r\n" +
				"business goals. The mission of WLN is to provide a\r\n" +
				"forum that empowers, develops, and connects a network\r\n" +
				"of women to drive business success. The group is open\r\n" +
				"to all Team Schein Members, regardless of gender and\r\n" +
				"level, and participation is encouraged from all",WordWrapper.wrap("   Women’s Leadership Network (WLN)is Henry Schein’s first Employee Resource Group (ERG), which is a voluntary, employee-led network that fosters a diverse, inclusive workplace aligned with the Company’s Corporate Charter, Team Schein Values, and business goals. The mission of WLN is to provide a forum that empowers, develops, and connects a network of women to drive business success. The group is open to all Team Schein Members, regardless of gender and level, and participation is encouraged from all     ", 54));
	}

}

