package source;

public class WordWrapper {

	public static String wrap(String text,int maxLineLength)
	{
		//Implementation
		//using string builders since they are mutable during iterative process and assignment
		StringBuilder strbuilder = new StringBuilder();

		if(text == null) {return new String("The given string is Null");}
		else if (text.length() <= 0) {return new String("The given string is Empty");}
		else {
			
			while(text.length() > 0)
			{
				String substring;
				//checking the text length is longer than the maxLineLength then break the text
				if(text.length() > maxLineLength)
				{
					//take the substring from the input string up to maxLineLength characters
					substring = text.substring(0,maxLineLength);
					int lastindexofspace = substring.lastIndexOf(" ");
					 /*In order to split the broken words hanging at the end
					 * we split the words at the boundaries, by moving back each character from
					 * the end of the split string until a space is found */
					substring = substring.substring(0,lastindexofspace);
					substring.trim();
					strbuilder.append(substring);
					strbuilder.append(System.getProperty("line.separator"));
					text = text.substring(lastindexofspace+1);

				}
				//checking the text length is not longer than the maxLineLength then no line breaks
				else
				{
					//removing white spaces at the leading and trailing edges of each sentence
					//substring = text.replaceAll("^[ \\t]+|[ \\t]+$", "");
					substring = text;
					substring.trim();
					strbuilder.append(substring);
					text = "";

				}


			}

		}
		return strbuilder.toString().replaceAll("^[ \\t]+|[ \\t]+$", "");
	}


}



